package sheridan;
import java.util.Scanner;

/**
 * This class holds the main() method
 * 
 * @author rafaelbattesti
 * @version 1.0
 * @since 2015-May-12
 */
public class SongApplication {

    private Song _beerSong;
    private int _originalCount;
    
    /**
     * Main method. Calls 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /* ProfM: The first line of the main method should always create an
        application object */
        SongApplication app = new SongApplication();
        app._originalCount = Integer.parseInt(args[0]);
        app._beerSong.setBottleCount(app._originalCount);
        app.run();  
    }
    
    public SongApplication() {
        _beerSong = new Song();
    }
    
    private void run() {
        
        Scanner input = new Scanner(System.in);
        String option;
        boolean quit = false;
        
        
        while(!quit) {
        
            for (int i = _beerSong.getBottleCount(); i >= 0; i--) {
                _beerSong.printSong();
            }
            
            System.out.println("Continue? [y]/[n]");
            option = input.nextLine();
            
            if (option.equalsIgnoreCase("y")) {
                quit = false;
                _beerSong.setBottleCount(_originalCount);
            } else {
                System.out.println("Goodbye! =)");
                System.exit(0);
            }
        
        }
    }
    
}
