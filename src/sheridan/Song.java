/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 * This class holds all the fields and logic to create a Song.
 * @author rafaelbattesti
 */
public class Song {
    
    private static final String WORD_SINGULAR = "bottle";
    private static final String WORD_PLURAL   = "bottles";
    
    private int _bottleCount;
    private int _originalCount;
    
    
    public Song() {
        
    }
    
    public Song(int bottleCount) {
        _bottleCount = bottleCount;
        
    }
    
    public int getBottleCount() {
        return _bottleCount;
    }
    
    public void setBottleCount(int bottleCount) {
        _bottleCount = bottleCount;
        _originalCount = bottleCount;
    }
    
    public void printSong() {
        
        String bottle = _bottleCount == 1 ? WORD_SINGULAR : WORD_PLURAL;
        
        if (_bottleCount != 0) {
            
            System.out.println(String.format("%d %s of beer on the wall, "
                    + "%d %s of beer.", _bottleCount, bottle, _bottleCount, 
                    bottle));
            System.out.print("Take one down and pass it around, ");
            
            _bottleCount--;
            bottle = _bottleCount == 1 ? WORD_SINGULAR : WORD_PLURAL;
            
            if (_bottleCount == 0) {
                System.out.println("no more bottles of beer on the wall.\n");
            } else {
                System.out.println(String.format("%d %s of beer on the wall.\n",
                        _bottleCount, bottle));
            }
            
        } else {
            System.out.println("No more bottles of beer on the wall, no more "
                    + "bottles of beer.");
            System.out.println(String.format("Go to the store and buy some "
                    + "more, %d %s of beer on the wall.\n",
                    _originalCount, bottle));
        }
    }
}
